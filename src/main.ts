import { createApp } from 'vue'
import './style.css'
import './script/Time';
import App from './App.vue'

createApp(App).mount('#app')

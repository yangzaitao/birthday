function getCssVar(var_: string) {
    const val = window.getComputedStyle(document.documentElement).getPropertyValue(var_);
    if (val.length > 0) {
        return val;
    }
    return undefined;
}
export { getCssVar };
enum IDType {
    TIME = 'time',
    RECEVIER = 'receiver',
    BTDY = 'btdy',
    CAKE = 'cake',
    FIRE = 'fire',
    CANDLE = 'candle',
}
export { IDType }
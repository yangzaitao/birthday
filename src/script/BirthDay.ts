import { IDType } from "./comm/IdType";
import CIR from "@/assets/cir.png"
import CLEVER from "@/assets/clever.png"
import BANK from "@/assets/bank.png";

class Img {
    getElement(url: string) {
        const img = document.createElement('img');
        img.src = url;
        return img;
    }
    static CIR = CIR;
    static BANK = BANK;
    static CLEVER = CLEVER;
}


class MoveIcon {
    iconNode: HTMLElement;
    constructor(icon: HTMLElement) {
        this.iconNode = document.createElement("div");
        this.iconNode.style.position = 'fixed';
        this.iconNode.style.pointerEvents = 'none';
        this.iconNode.id = 'ig';
        this.iconNode
        document.addEventListener('pointermove', this.mousemove.bind(this));
        document.body.appendChild(this.iconNode);
    }
    mousemove(evt: PointerEvent) {
        const pos = this.iconNode.getClientRects()[0]
        this.iconNode.style.top = `${evt.pageY - (pos.height * 0.5)}px`;
        this.iconNode.style.left = `${evt.pageX - (pos.width * 0.5)}px`;
    }
    clear() {
        document.body.querySelector("#ig")?.remove();
    }
}

class BirthDay {
    btdy: HTMLElement;
    cake: HTMLElement;
    fire: HTMLElement;
    candle: HTMLElement;
    constructor() {
        this.btdy = document.getElementById(IDType.BTDY)!;
        this.cake = document.getElementById(IDType.CAKE)!;
        this.fire = document.getElementById(IDType.FIRE)!;
        this.candle = document.getElementById(IDType.CANDLE)!;
        this.init();
    }
    init() {
        // new MoveIcon();
    }
    show() {
        this.btdy.style.visibility = "visible";
    }
    hide() {
        this.btdy.style.visibility = "visible";
    }
}

export { BirthDay };
import * as echarts from 'echarts';
import { IDType } from './comm/IdType';
class Recevier {
    private dom: HTMLElement | null;
    private myChart: echarts.ECharts;
    constructor(domid: IDType) {
        this.dom = document.getElementById(domid)!;
        this.myChart = echarts.init(this.dom);
        if (this.dom === null) {
            console.error(Recevier.name, '元素没找到！');
            return;
        }
    }
    private update(data: number[]) {
        const option: echarts.EChartsOption = {
            grid: { left: '0', bottom: '0', right: '0', top: '0' },
            xAxis: {
                type: 'category',
            },
            yAxis: {
                splitLine: {
                    lineStyle: {
                        color: 'rgb(255, 70, 131)'
                    }
                }
            },
            series: [
                {
                    data: data,
                    type: 'line',
                    symbol: 'none',
                    smooth: true,
                    lineStyle: {
                        width: 1,
                    },
                    itemStyle: {
                        color: 'rgb(255, 70, 131)'
                    },
                }
            ]
        };
        option && this.myChart.setOption(option);
    }
    private static recevier: Recevier;
    static start() {
        this.recevier = new Recevier(IDType.RECEVIER);
        setInterval(() => {
            const data: number[] = [];
            for (let i = 0; i < 50; i++) {
                data.push((Math.random() * 1000))
            }
            this.recevier.update(data);
        }, 500)
    }
}

export { Recevier };
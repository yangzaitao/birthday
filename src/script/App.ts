import { getCssVar } from './comm/Style';
import { Recevier } from './Recevier';
import { time } from './Time';
import { BirthDay } from './BirthDay';
class App {
    constructor(readonly dom: HTMLElement) {
        this.init();
    }
    init() {
        new BirthDay();
    }
}
export { App };